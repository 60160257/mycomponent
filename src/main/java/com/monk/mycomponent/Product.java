/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author acer
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product>list = new ArrayList<>();
        list.add(new Product(1,"Espresso 1",30,"Espresso-1.jpg"));
        list.add(new Product(2,"Espresso 2",40,"Espresso-1.jpg"));
        list.add(new Product(3,"Espresso 3",50,"Espresso-1.jpg"));
        list.add(new Product(1,"Cappuccino 1",30,"cappuccino-2.jpg"));
        list.add(new Product(2,"Cappuccino 2",40,"cappuccino-2.jpg"));
        list.add(new Product(3,"Cappuccino 3",50,"cappuccino-2.jpg"));
        list.add(new Product(1,"Latte 1",30,"Latte-1.jpg"));
        list.add(new Product(2,"Latte 2",40,"Latte-1.jpg"));
        list.add(new Product(3,"Latte 3",50,"Latte-1.jpg"));
        return list;
        
    }
}
